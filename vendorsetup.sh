#
# Copyright 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#main
export ALLOW_MISSING_DEPENDENCIES=true
export LC_ALL="C"
export FOX_VERSION=R11.0_20210405
export FOX_BUILD_TYPE=Unofficial
export OF_MAINTAINER=Adou2056
export OF_SCREEN_H=2340
export OF_STATUS_H=94
export OF_STATUS_INDENT_LEFT=48
export OF_STATUS_INDENT_RIGHT=48
export FOX_R11=1


#A/B device
export OF_AB_DEVICE=1
export OF_TARGET_DEVICE="judyln"
export TARGET_DEVICE="judyln"

#special
export OF_FLASHLIGHT_ENABLE=1
export OF_CLOCK_POS=1
export OF_DISABLE_MIUI_SPECIFIC_FEATURES=1
export OF_DISABLE_DM_VERITY_FORCED_ENCRYPTION=1
export OF_NO_SAMSUNG_SPECIAL=1
export OF_FLASHLIGHT_ENABLE=1
export OF_SUPPORT_PRE_FLASH_SCRIPT=1
export OF_SUPPORT_OZIP_DECRYPTION=1
export OF_QUICK_BACKUP_LIST="/data;/storage;/persist;"
export OF_USE_TWRP_SAR_DETECT=1
export OF_USE_MAGISKBOOT=1
export OF_USE_MAGISKBOOOFT_FOR_ALL_PATCHES=1
export FOX_USE_TWRP_RECOVERY_IMAGE_BUILDER=1
export FOX_USE_NANO_EDITOR=1
export FOX_LED_COLOR=1
export FOX_DELETE_AROMAFM=1
export FOX_DELETE_INITD_ADDON=1
export FOX_DELETE_REBOOT_DOWNLOAD=1
export FOX_DELETE_REBOOT_EDL=1
export FOX_RESET_SETTINGS=1
export FOX_PRE_FLASH_SCRIPT=1

#use ccache
export USE_CCACHE.=1
export CCACHE_DIR=/mnt/d/wsl/.ccache

  # add lunch
  add_lunch_combo omni_judyln-eng
